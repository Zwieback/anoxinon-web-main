+++ 
date = "2020-10-31T15:00:00+01:00" 
draft = false 
author = "Anoxinon" 
title = "Förderung von FOSS-Projekten" 
description = "Vorschläge einreichen" 
categories = ["Verein", "Open Source"] 
tags = ["Open Source"] 
+++ 

Hallo liebe Freunde von Free Open Source Software!

Wir möchten dieses Jahr ein FOSS-Projekt finanziell ein wenig unterstützen.

Da unsere monetären Mittel leider endlich sind und es zu viele tolle Entwickler und Projekte gibt, möchten wir eure "Favoriten" in Erfahrung bringen.

Bis zum 01.12.2020 könnt ihr uns eure Top 3 Vorschläge zukommen lassen. Schickt uns eine E-Mail an foss@anoxinon.de oder schreibt uns auf [Mastodon](https://social.anoxinon.de/@Anoxinon).

Danach werden alle Mails und Toots ausgewertet und das Projekt mit den meisten Stimmen vorgestellt. Im Anschluss wird es einen Spendenaufruf geben welchem ihr hoffentlich in großer Zahl folgen werdet.

Jede Spende in diesem Zeitraum kommt dann direkt dem jeweiligen Projekt bzw. Entwickler zugute. Der Verein wird aus Eigenmitteln noch 500 € drauflegen. Die Auszahlung erfolgt bis Weihnachten 2020.

Vielen Dank für Eure Unterstützung!

Team Anoxinon