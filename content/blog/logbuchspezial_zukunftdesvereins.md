+++
date = "2019-12-20T15:00:00+01:00"
draft = false
author = "Anoxinon"
title = "Logbuch Spezial - Zukunft des Vereins"
description = "Fortbestand des Vereins in Schwebe"
categories = ["allgemeines", "verein", "dienste"]
tags = ["Vereinsangelegenheiten","rss", "Website", "Verein"]

+++
**Update zum 29.12.2019**
Auf der heutigen Mitgliederversammlung wurde ein neuer Vorstand gewählt. Der Fortbestand des Vereins ist also gesichert. Weitere Informationen gibt es die Tage in einem weiteren Blogbeitrag.

---
**Update zum 27.12.2019**

Derzeit laufen Gespräche mit einer Interessierten Person, dennoch sind wir weiter auf der Suche nach Personen die uns im Vorstand unterstützen wollen würden. Solltet Ihr euch berufen fühlen, oder etwaige Fragen haben, meldet euch doch einfach per E-Mail an postfach@anoxinon.de. :)

An dieser Stelle sei noch erwähnt, dass der Verein noch relativ jung ist. Einiges, wie die neue Homepage und dem zugehörigen Update der Inhalte, ist aufgrund der verringerten Kapazitäten vertagt worden. Es muss also noch vieles weiter forciert und angegangen werden. Das entsprechende neue Design, ist bspw. schon fast fertiggestellt. 

Wir halten euch weiter auf dem Laufenden.

---
*Beitrag vom 20.12.2019*

Liebe Leser und Interessierte,

Der Anoxinon e.V. befindet sich aktuell in einer schwierigen Lage, die die Zukunft des Vereins aktuell ungewiss erscheinen lässt.

Unser Vorstandsvositzende wird aus wichtigen gesundheitlichen Gründen zum 31.12.2019 von seinem Amt zurücktreten und hat die übrigen Vorstandsmitglieder am 04.12.2019 darüber informiert. So verbleiben von aktuell drei Vorstandsmitglieder nur noch zwei.

Nach reichlicher Überlegung werden auch die übrigen beiden Vorstandsmitglieder zum 19.01.2020 von ihren Ämtern zurücktreten. Der Vorstand ist sich darüber bewusst, dass dieser Schritt den Verein in eine schwierige Situation bringt. Nicht zuletzt aus beruflichen Gründen können die übrigen Vorstandsmitglieder die Arbeitslast jedoch schon seit längerer Zeit nicht mehr stemmen, und sehen sich gezwungen, das Engagement im Verein zu reduzieren.

In der außerordentlichen Mitgliederversammlung am 10.11.2019 wurde bereits eine Diskussion über die Zukunft des Vereins geführt und dazu aufgerufen, einen neuen Vorstandsvorsitzenden zu benennen. Zum damaligen Zeitpunkt konnte jedoch noch keine passender Kandidat gefunden werden.

**Nun, da auch der übrige Vorstand zurückgetreten ist - sucht der Anoxinon e.V. dringend nach neuen Vorstandsmitgliedern bzw. einem Vorstandsvorsitzenden.**

Aus diesem Grund ist für den 29.12.2019 und den 19.01.2020 eine Mitgliederversammlung angekündigt, in der mögliche Kandidaten für die Vorstandsmitgliedschaft gewählt werden sollen. Wir werden bis dahin weiter intensiv nach geeigneten Kandidaten suchen - sowohl intern unter den bereits bestehenden Mitgliedern - als auch öffentlich.


**Sollte in der Versammlung am 19.01.2020 nicht mindestens ein neues Vorstandsmitglied gewählt werden, ist der Verein vorstandslos und damit nur sehr eingeschränkt Handlungsfähig. Eine Auflösung des Vereins steht dann bevor.**

Dies beinhaltet die **umgehende Einstellung** sämlicher vom Verein angebotenen Dienste, z.B. XMPP-Server, Mastodon-Server, Mumble-Server, Blog und mehr. Sollte bis Ende des Monats noch kein Kandidat gefunden werden, wird der Verein die Registration für Mastodon und XMPP deaktivieren und die Nutzer noch einmal über die Situation informieren.

Daher noch einmal explizit der Aufruf: **Solltest du dich mit der Rolle als Vereinsvorstand des Anoxinon e.V. identifizieren können, bewirb' dich bitte unkompliziert via E-Mail: postfach@anoxinon.de**. Wir suchen Dich!

Wir möchten noch erwähnen dass niemand alleine gelassen wird. Die bisherigen Vorstandsmitglieder können sich vorstellen weiterhin Ihre Arbeit fortzusetzen, wenn es jemanden gibt der die Arbeitslast mittragen kann und die Aktivität des Vereins sicherstellt. ___Eine interessierte Person würde alle Unterstützung erhalten die notwendig ist.___ Auch jemand mit Wenig Vorwissen ist willkommen. Wichtigste Eigenschaft ist die Motiviation.

Aufgaben eines Vereinsvorstands / Vorstandsvorsitzenden beinhalten beispielsweise:

* Koordination der Vereinstätigkeiten
* Bestimmung und Aufnahme neuer Vereinsmitglieder
* Repräsentation des Vereins in der Öffentlichkeit
* Kommunikation mit Behörden, Ämtern, Finanz- und Versicherungsinstituten
* Sicherstellen der Aktivität des Vereins

Übrige Rechte und Aufgaben können der Vereinssatzung entnommen werden: https://anoxinon.de/files/Satzung.pdf

Wir werden euch diesbezüglich weiter am laufenden halten.
