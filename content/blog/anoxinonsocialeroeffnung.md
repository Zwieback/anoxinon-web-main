+++
date = "2018-07-07T14:59:59+01:00"
title = "Eröffnung Anoxinon Social"
tags = ["mastodon","rss","anoxinon","fediverse"]
categories = ["dienste"]
author = "Anoxinon"
draft = false
description = "Start der Mastodon Instanz"
+++

Hört, Hört werte Leut, wir haben etwas zu verkünden.<br>

Lange geplant, vor wenigen Tagen endlich umgesetzt, ist unsere Mastodon Instanz nun für die Öffentlichkeit verfügbar.
Mit diesem Schritt möchten wir das Fediverse weiter stärken, unterstützen und zum Wachstum beitragen.
Der Grundgedanke hinter einem dezentralen Netzwerk passt definitiv in unser Portfolio. Ausserdem bietet das Fediverse, aus unserer Sicht, mehr als vergleichsweise zentral gesteuerte Dienste. (Zwitscher, FB etc.)<br>


Ihr fragt euch was Mastodon ist?
<a href="https://anoxinon.de/dienste/anoxinonsocial/"> Klickt hier für mehr Infos!</a>

Der Link zu unserer Instanz lautet <a href="https://social.anoxinon.de">social.anoxinon.de</a>.

Bei Fragen könnt Ihr uns, wie immer, gerne kontaktieren!

Grüße.
<br>
euer Anoxinon Team
