+++
date = "2019-01-02T15:20:00+01:00"
author = "Anoxinon"
title = "Eröffnung Anoxinon Media"
description = "Start der Content Plattform"
categories = ["Allgemeines", "Verein", "Dienste"]
tags = ["anoxinon","media","Content", "Website"]
+++

Hallo zusammen,

endlich ist es soweit: Unser Blog "Anoxinon Media" ist ab heute online.

Auf dieser Website möchten wir interessierte Nutzer über unsere Themenbereiche wie Datenschutz und Verschlüsselung aufklären und informieren. Damit realisieren wir, neben dem Angebot von öffentlich verfügbaren Diensten wie Mastodon und XMPP, unser zweites Standbein.  

Ein großer Teil der Bevölkerung hat sich leider noch keine Gedanken um die potenziellen Gefahren und Möglichkeiten der Technik gemacht. Auf manche grundsätzlichen Fragen des Datenschutzes und der informationellen Selbstbestimmung haben viele Menschen noch keine Antwort gefunden. Wir möchten uns als Anlaufstelle etablieren, die zielgruppengerecht die Thematiken einfach und verständlich aufschlüsselt sowie zum Nachdenken anregt.

In regelmäßigen Intervallen werden wir Beiträge veröffentlichen, derzeit sind zwei pro Monat angedacht. In Zukunft möchten wir unser Angebot erweitern, dafür sind wir noch auf der Suche nach [motivierten Helfern](/stellenboerse/). :)

Den Start macht jeweils ein Beitrag für Anfänger über Datenschutz und Freie Software.  
Mehr über Anoxinon Media erfahrt ihr auf [dieser FAQ Seite](https://anoxinon.media/faq).

Wir freuen uns auf euer Feedback!
